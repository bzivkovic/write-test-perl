use Time::HiRes qw/ time /;

$file = "/Volumes/RamDisk/output.txt";
print "\nWriting $file\n";

$text = "One row dsfgsdfgsdgh sdhg sthsthhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhhh\n";
$start = time();

open (w,">".$file) || die "Can not open $file\n";

$i = 1;
do {
	print w $text;
	$i++;
	} while ($i < 10000000);

# for ($i=1; $i < 10000000; $i++) {
#     print w $text;
# }
close(w);

$end = time();
$time_elapsed = $end - $start;  
print "\n------------------------------------------------------------------------------------
Time it took to write 10,000,000 lines to 1 file: |".$time_elapsed."| sec.\n";

$filesize = -s $file;
$filesize = $filesize / (1024 * 1024);
print "Filesize: |$filesize| mb.\n";

$speed = $filesize / $time_elapsed;
print "Speed of writing 1 file continuisly: |$speed| mb/sec.
------------------------------------------------------------------------------------\n";